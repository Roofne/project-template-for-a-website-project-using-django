from django import views
from django.contrib import admin
from django.urls import include, path

from . import views

urlpatterns = [
    path('api/', views.getAll, name='api'),
    path('api/{id}', views.get, name='api'),
]